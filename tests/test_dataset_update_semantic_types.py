import unittest

from common_primitives import dataset_to_dataframe, dataset_update_semantic_types
from d3m.metadata import base as metadata_base

import utils as test_utils


class UpdateSemanticTypesPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):
        # load the iris dataset
        new_dataset = self._loadAndUpdate()

        # validate metadata
        semantic_types = new_dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS, 1))['semantic_types']
        self.assertSetEqual({'http://schema.org/Text', 'http://schema.org/Integer', 'https://metadata.datadrivendiscovery.org/types/Attribute'}, set(semantic_types))

        semantic_types = new_dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS, 3))['semantic_types']
        self.assertSetEqual({'https://metadata.datadrivendiscovery.org/types/Attribute'}, set(semantic_types))

    def test_with_convert(self):
        # convert to dataframe
        dataset = self._loadAndUpdate()
        ds2df_hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        ds2df_primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=ds2df_hyperparams_class.defaults())
        dataframe = ds2df_primitive.produce(inputs=dataset).value

        # sanity check the conversion
        semantic_types = dataframe.metadata.query((metadata_base.ALL_ELEMENTS, 1))['semantic_types']
        self.assertSetEqual({'http://schema.org/Text', 'http://schema.org/Integer', 'https://metadata.datadrivendiscovery.org/types/Attribute'}, set(semantic_types))

        semantic_types = dataframe.metadata.query((metadata_base.ALL_ELEMENTS, 3))['semantic_types']
        self.assertSetEqual({'https://metadata.datadrivendiscovery.org/types/Attribute'}, set(semantic_types))

    def _loadAndUpdate(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        update_types_hyperparams_class = dataset_update_semantic_types.UpdateSemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        hyperparams = update_types_hyperparams_class({
            'resource_id': '0',
            'add_columns': (1,),
            'add_types': ('http://schema.org/Text', 'http://schema.org/Integer'),
            'remove_columns': (1, 3),
            'remove_types': ('http://schema.org/Float',)
        })
        update_types_primitive = dataset_update_semantic_types.UpdateSemanticTypesPrimitive(hyperparams=hyperparams)
        new_dataset = update_types_primitive.produce(inputs=dataset).value

        return new_dataset

    def test_can_accept(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        dataset_metadata = dataset.metadata.set_for_value(None)

        update_types_hyperparams_class = dataset_update_semantic_types.UpdateSemanticTypesPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        hyperparams = update_types_hyperparams_class({
            'resource_id': '0',
            'add_columns': (1,),
            'add_types': ('http://schema.org/Text', 'http://schema.org/Integer'),
            'remove_columns': (1, 3),
            'remove_types': ('http://schema.org/Float',)
        })

        new_dataset_metadata = dataset_update_semantic_types.UpdateSemanticTypesPrimitive.can_accept(method_name='produce', arguments={'inputs': dataset_metadata}, hyperparams=hyperparams)

        # validate metadata
        semantic_types = new_dataset_metadata.query(('0', metadata_base.ALL_ELEMENTS, 1))['semantic_types']
        self.assertSetEqual({'http://schema.org/Text', 'http://schema.org/Integer', 'https://metadata.datadrivendiscovery.org/types/Attribute'}, set(semantic_types))

        semantic_types = new_dataset_metadata.query(('0', metadata_base.ALL_ELEMENTS, 3))['semantic_types']
        self.assertSetEqual({'https://metadata.datadrivendiscovery.org/types/Attribute'}, set(semantic_types))


if __name__ == '__main__':
    unittest.main()
