import json
import os
import unittest

from d3m import container, utils
from d3m.metadata import base as metadata_base

from common_primitives import denormalize


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=utils.JsonEncoder))


class DenormalizePrimitiveTestCase(unittest.TestCase):
    def test_discard(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset_metadata_before = dataset.metadata.to_json_structure()

        hyperparams_class = denormalize.DenormalizePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = denormalize.DenormalizePrimitive(hyperparams=hyperparams_class.defaults().replace({
            'recursive': False,
            'discard_not_joined_tabular_resources': True,
        }))

        denormalized_dataset = primitive.produce(inputs=dataset).value

        self.assertIsInstance(denormalized_dataset, container.Dataset)

        self.assertIs(denormalized_dataset, denormalized_dataset.metadata.for_value)

        self.assertEqual(len(denormalized_dataset), 1)

        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 1]), {'AAA', 'BBB', 'CCC'})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 2]), {'AAA name', 'BBB name', 'CCC name'})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 3]), {'1', '2', ''})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 4]), {'aaa', 'bbb', 'ccc', 'ddd', 'eee'})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 5]), {'1990', '2000', '2010'})

        self._test_discard_metadata(denormalized_dataset.metadata, dataset_doc_path)

        self.assertEqual(dataset.metadata.to_json_structure(), dataset_metadata_before)

    def _test_discard_metadata(self, metadata, dataset_doc_path):
        self.maxDiff = None

        self.assertEqual(convert_metadata(metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'database_dataset_1',
            'version': '1.0',
            'name': 'A dataset simulating a database dump',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'dimension': {
                'name': 'resources',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/DatasetResource',
                ],
                'length': 1,
            },
            'digest': '873329de7f25132362a43e76e7376dd534b3f7341fce9e17b1769f949badb0d1',
            'description': 'A synthetic dataset trying to be similar to a database dump, with tables with different relations between them.',
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
        })

        self.assertEqual(convert_metadata(metadata.query(('3',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/TabularRow',
                ],
                'length': 45,
            },
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS,))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 7,
            }
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 3))), {
            'name': 'author',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
            'foreign_key': {
                'type': 'COLUMN',
                'resource_id': '1',
                'column_index': 0,
            },
        })

        for i in [1, 2, 4]:
            self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['code', 'name', None, 'key'][i - 1],
                'structural_type': 'str',
                'semantic_types': [
                    'http://schema.org/Text',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 5))), {
            'name': 'year',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Time',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 6))), {
            'name': 'value',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

    def test_recursive(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset_metadata_before = dataset.metadata.to_json_structure()

        hyperparams_class = denormalize.DenormalizePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = denormalize.DenormalizePrimitive(hyperparams=hyperparams_class.defaults().replace({
            'recursive': True,
            'discard_not_joined_tabular_resources': False,
        }))

        denormalized_dataset = primitive.produce(inputs=dataset).value

        self.assertIsInstance(denormalized_dataset, container.Dataset)

        self.assertIs(denormalized_dataset, denormalized_dataset.metadata.for_value)

        self.assertEqual(len(denormalized_dataset), 4)

        self.assertEqual(denormalized_dataset['2'].shape[0], 64)
        self.assertEqual(denormalized_dataset['3'].shape[1], 8)

        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 1]), {'AAA', 'BBB', 'CCC'})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 2]), {'AAA name', 'BBB name', 'CCC name'})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 3]), {'1', '2', ''})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 4]), {'1 name', '2 name', ''})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 5]), {'aaa', 'bbb', 'ccc', 'ddd', 'eee'})
        self.assertEqual(set(denormalized_dataset['3'].iloc[:, 6]), {'1990', '2000', '2010'})

        self._test_recursive_metadata(denormalized_dataset.metadata, dataset_doc_path)

        self.assertEqual(dataset.metadata.to_json_structure(), dataset_metadata_before)

    def _test_recursive_metadata(self, metadata, dataset_doc_path):
        self.maxDiff = None

        self.assertEqual(convert_metadata(metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'database_dataset_1',
            'version': '1.0',
            'name': 'A dataset simulating a database dump',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'dimension': {
                'name': 'resources',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/DatasetResource',
                ],
                'length': 4,
            },
            'digest': '873329de7f25132362a43e76e7376dd534b3f7341fce9e17b1769f949badb0d1',
            'description': 'A synthetic dataset trying to be similar to a database dump, with tables with different relations between them.',
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
        })

        self.assertEqual(convert_metadata(metadata.query(('3',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/TabularRow',
                ],
                'length': 45,
            },
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS,))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 8,
            }
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 3))), {
            'name': 'id',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

        for i in [1, 2, 4, 5]:
            self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['code', 'name', None, 'name', 'key'][i - 1],
                'structural_type': 'str',
                'semantic_types': [
                    'http://schema.org/Text',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 6))), {
            'name': 'year',
            'structural_type': 'str',
            'semantic_types': [
                "https://metadata.datadrivendiscovery.org/types/Time",
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('3', metadata_base.ALL_ELEMENTS, 7))), {
            'name': 'value',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'
            ],
        })

    def test_row_order(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'image_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        dataset_metadata_before = dataset.metadata.to_json_structure()

        hyperparams_class = denormalize.DenormalizePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = denormalize.DenormalizePrimitive(hyperparams=hyperparams_class.defaults().replace({
            'recursive': True,
            'discard_not_joined_tabular_resources': False,
        }))

        denormalized_dataset = primitive.produce(inputs=dataset).value

        self.assertIsInstance(denormalized_dataset, container.Dataset)

        self.assertIs(denormalized_dataset, denormalized_dataset.metadata.for_value)

        self.assertEqual(len(denormalized_dataset), 1)

        self.assertEqual(denormalized_dataset['1'].shape, (5, 3))

        self.assertEqual(denormalized_dataset['1'].values.tolist(), [
            ['0', 'mnist_0_2.png', 'mnist'],
            ['1', 'mnist_1_1.png', 'mnist'],
            ['2', '001_HandPhoto_left_01.jpg', 'handgeometry'],
            ['3', 'cifar10_bird_1.png', 'cifar'],
            ['4', 'cifar10_bird_2.png', 'cifar'],
        ])

        self._test_row_order_metadata(denormalized_dataset.metadata, dataset_doc_path)

        self.assertEqual(dataset.metadata.to_json_structure(), dataset_metadata_before)

    def _test_row_order_metadata(self, metadata, dataset_doc_path):
        self.maxDiff = None

        self.assertEqual(convert_metadata(metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'image_dataset_1',
            'version': '1.0',
            'name': 'Image dataset to be used for tests',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'dimension': {
                'name': 'resources',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/DatasetResource',
                ],
                'length': 1,
            },
            'digest': '14afad12884e945acc25464a8889071b5f007874afb726da23b8b58c19b561f9',
            'description': 'There are a total of 5 image files, one is a left hand from the handgeometry dataset, two birds from cifar10 dataset and 2 figures from mnist dataset.',
            'source': {
                'license': 'Creative Commons Attribution-NonCommercial 4.0',
                'redacted': False,
                'human_subjects_research': False,
            },
            'approximate_stored_size': 24000,
        })

        self.assertEqual(convert_metadata(metadata.query(('1',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/TabularRow',
                ],
                'length': 5,
            },
        })

        self.assertEqual(convert_metadata(metadata.query(('1', metadata_base.ALL_ELEMENTS,))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 3,
            }
        })

        self.assertEqual(convert_metadata(metadata.query(('1', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('1', metadata_base.ALL_ELEMENTS, 1))), {
            'name': 'filename',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/FileName',
                'http://schema.org/ImageObject',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
            'location_base_uris': [
                'file://{dataset_base_path}/media/'.format(dataset_base_path=os.path.dirname(dataset_doc_path)),
            ],
            'media_types': [
                'image/jpeg',
                'image/png',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('1', metadata_base.ALL_ELEMENTS, 2))), {
            'name': 'class',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('1', 0, 1))), {
            'name': 'filename',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/FileName',
                'http://schema.org/ImageObject',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
            'location_base_uris': [
                'file://{dataset_base_path}/media/'.format(dataset_base_path=os.path.dirname(dataset_doc_path)),
            ],
            'media_types': [
                'image/png',
            ],
        })

        self.assertEqual(convert_metadata(metadata.query(('1', 2, 1))), {
            'name': 'filename',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/FileName',
                'http://schema.org/ImageObject',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
            'location_base_uris': [
                'file://{dataset_base_path}/media/'.format(dataset_base_path=os.path.dirname(dataset_doc_path)),
            ],
            'media_types': [
                'image/jpeg',
            ],
        })


if __name__ == '__main__':
    unittest.main()
