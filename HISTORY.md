## vNEXT

* Simplified logic (and made it more predictable) of `combine_columns` utility function when
  using `new` `return_result` and `add_index_columns` set to true. Now if output already contains
  any index column, input index columns are not added. And if there are no index columns,
  all input index columns are added at the beginning.
* Fixed `_can_use_inputs_column` in `classifier.RandomForest`. Added check of structural type, so
  only columns with numerical structural types are processed.
* Correctly set column names in `evaluation.ComputeScores` primitive's output.
* Cast indices and columns to match predicted columns' dtypes.
  [#33](https://gitlab.com/datadrivendiscovery/common-primitives/issues/33)
* `datasets.DatasetToDataFrame` primitive does not try to generate metadata automatically
  because this is not really needed (metadata can just be copied from the dataset). This
  speeds up the primitive.
  [#34](https://gitlab.com/datadrivendiscovery/common-primitives/issues/34)
* Made it uniform that whenever we are generating lists of all column names
  we try first to get the name from the metadata and fallback to one in DataFrame.
  Instead of using a column index in the latter case.
* Made splitting primitives, `classifier.RandomForest` and `data.UnseenLabelEncoder`
  be picklable even unfitted.
* Fixed entry point for `audio.CutAudio` primitive.

## v0.2.0

* Made those primitives operate on semantic types and support different ways to return results.
* Added or updated many primitives:
  * `data.ExtractColumns`
  * `data.ExtractColumnsBySemanticTypes`
  * `data.ExtractColumnsByStructuralTypes`
  * `data.RemoveColumns`
  * `data.RemoveDuplicateColumns`
  * `data.HorizontalConcat`
  * `data.CastToType`
  * `data.ColumnParser`
  * `data.ConstructPredictions`
  * `data.DataFrameToNDArray`
  * `data.NDArrayToDataFrame`
  * `data.DataFrameToList`
  * `data.ListToDataFrame`
  * `data.NDArrayToList`
  * `data.ListToNDArray`
  * `data.StackNDArrayColumn`
  * `data.AddSemanticTypes`
  * `data.RemoveSemanticTypes`
  * `data.ReplaceSemanticTypes`
  * `data.UnseenLabelEncoder`
  * `data.UnseenLabelDecoder`
  * `data.ImageReader`
  * `data.TextReader`
  * `data.VideoReader`
  * `data.CSVReader`
  * `data.AudioReader`
  * `datasets.Denormalize`
  * `datasets.DatasetToDataFrame`
  * `datasets.UpdateSemanticTypes`
  * `datasets.RemoveColumns`
  * `evaluation.RedactTargets`
  * `evaluation.ComputeScores`
  * `evaluation.KFoldDatasetSplit`
  * `evaluation.TrainScoreDatasetSplit`
  * `audio.CutAudio`
  * `classifier.RandomForest`
* Starting list enabled primitives in the [`entry_points.ini`](./entry_points.ini) file.
* Created `devel` branch which contains primitives coded against the
  future release of the `d3m` core package (its `devel` branch).
  `master` branch of this repository is made against the latest stable
  release of the `d3m` core package.
* Dropped support for Python 2.7 and require Python 3.6.
* Renamed repository and package to `common-primitives` and `common_primitives`,
  respectively.
* Repository migrated to gitlab.com and made public.

## v0.1.1

* Made common primitives work on Python 2.7.

## v0.1.0

* Initial set of common primitives.
