import os
import typing

import numpy  # type: ignore

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('ConstructPredictionsPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If metadata reconstruction happens, this is used for reference columns."
                    " If any specified column is not a primary index or a predicted target, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. If metadata reconstruction happens, this is used for reference columns. Applicable only if \"use_columns\" is not provided.",
    )


class ConstructPredictionsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which takes as input a DataFrame and outputs a DataFrame in Lincoln Labs predictions
    format: first column is a d3mIndex column (and other primary index columns, e.g., for object detection
    problem), and then predicted targets, each in its column, followed by optional confidence column(s).

    It supports both input columns annotated with semantic types (``https://metadata.datadrivendiscovery.org/types/PrimaryKey``,
    ``https://metadata.datadrivendiscovery.org/types/PredictedTarget``,
    ``https://metadata.datadrivendiscovery.org/types/Confidence``), or trying to reconstruct metadata.
    This is why the primitive takes also additional input of a reference DataFrame which should
    have metadata to help reconstruct missing metadata. If metadata is missing, the primitive
    assumes that all ``inputs`` columns are predicted targets, without confidence column(s).
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '8d38b340-f83f-4877-baaa-162f8e551736',
            'version': '0.3.0',
            'name': "Construct pipeline predictions output",
            'python_path': 'd3m.primitives.data.ConstructPredictions',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, reference: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        index_columns = utils.get_index_columns(inputs.metadata)
        target_columns = utils.list_columns_with_semantic_types(inputs.metadata, ('https://metadata.datadrivendiscovery.org/types/PredictedTarget',))

        if index_columns and target_columns:
            outputs = self._produce_using_semantic_types(inputs, index_columns, target_columns, self.hyperparams)
        else:
            outputs = self._produce_reconstruct(inputs, reference, index_columns, target_columns, self.hyperparams)

        outputs = self._encode_columns(outputs)

        # Generally we do not care about column names in DataFrame itself (but use names of columns from metadata),
        # but in this case setting column names makes it easier to assure that "to_csv" call produces correct output.
        # See: https://gitlab.com/datadrivendiscovery/d3m/issues/147
        column_names = []
        for column_index in range(len(outputs.columns)):
            column_names.append(outputs.metadata.query_column(column_index).get('name', outputs.columns[column_index]))
        outputs.columns = column_names

        return base.CallResult(outputs)

    @classmethod
    def _filter_index_columns(cls, inputs_metadata: metadata_base.DataMetadata, index_columns: typing.Sequence[int], hyperparams: Hyperparams) -> typing.Sequence[int]:
        if hyperparams['use_columns']:
            index_columns = [index_column_index for index_column_index in index_columns if index_column_index in hyperparams['use_columns']]
            if not index_columns:
                raise ValueError("No index columns listed in \"use_columns\" hyper-parameter, but index columns are required.")

        else:
            index_columns = [index_column_index for index_column_index in index_columns if index_column_index not in hyperparams['exclude_columns']]
            if not index_columns:
                raise ValueError("All index columns listed in \"exclude_columns\" hyper-parameter, but index columns are required.")

        names = []
        for index_column in index_columns:
            index_metadata = inputs_metadata.query_column(index_column)
            # We do not care about empty strings for names either.
            if index_metadata.get('name', None):
                names.append(index_metadata['name'])

        if 'd3mIndex' not in names:
            raise ValueError("\"d3mIndex\" index column is missing.")

        names_set = set(names)
        if len(names) != len(names_set):
            duplicate_names = names
            for name in names_set:
                # Removes just the first occurrence.
                duplicate_names.remove(name)

            cls.logger.warning("Duplicate names for index columns: %(duplicate_names)s", {
                'duplicate_names': list(set(duplicate_names)),
            })

        return index_columns

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, index_columns: typing.Sequence[int], target_columns: typing.Sequence[int], hyperparams: Hyperparams) -> typing.List[int]:
        assert index_columns
        assert target_columns

        index_columns = cls._filter_index_columns(inputs_metadata, index_columns, hyperparams)

        if hyperparams['use_columns']:
            target_columns = [target_column_index for target_column_index in target_columns if target_column_index in hyperparams['use_columns']]
            if not target_columns:
                raise ValueError("No target columns listed in \"use_columns\" hyper-parameter, but target columns are required.")

        else:
            target_columns = [target_column_index for target_column_index in target_columns if target_column_index not in hyperparams['exclude_columns']]
            if not target_columns:
                raise ValueError("All target columns listed in \"exclude_columns\" hyper-parameter, but target columns are required.")

        assert index_columns
        assert target_columns

        return list(index_columns) + list(target_columns)

    @classmethod
    def _get_confidence_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: Hyperparams) -> typing.List[int]:
        confidence_columns = utils.list_columns_with_semantic_types(inputs_metadata, ('https://metadata.datadrivendiscovery.org/types/Confidence',))

        if hyperparams['use_columns']:
            confidence_columns = [confidence_column_index for confidence_column_index in confidence_columns if confidence_column_index in hyperparams['use_columns']]
        else:
            confidence_columns = [confidence_column_index for confidence_column_index in confidence_columns if confidence_column_index not in hyperparams['exclude_columns']]

        return confidence_columns

    def _produce_using_semantic_types(self, inputs: Inputs, index_columns: typing.Sequence[int],
                                      target_columns: typing.Sequence[int], hyperparams: Hyperparams) -> Outputs:
        confidence_columns = self._get_confidence_columns(inputs.metadata, hyperparams)

        output_columns = self._get_columns(inputs.metadata, index_columns, target_columns, hyperparams) + confidence_columns

        # "get_index_columns" makes sure that "d3mIndex" is always listed first.
        # And "select_columns" selects columns in order listed, which then
        # always puts "d3mIndex" first.
        outputs = utils.select_columns(inputs, output_columns, source=self)

        if confidence_columns:
            outputs.metadata = self._update_confidence_columns(outputs.metadata, confidence_columns, self)

        return outputs

    @classmethod
    def _update_confidence_columns(cls, inputs_metadata: metadata_base.DataMetadata, confidence_columns: typing.Sequence[int], source: typing.Any) -> metadata_base.DataMetadata:
        output_columns_length = inputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        outputs_metadata = inputs_metadata

        # All confidence columns have to be named "confidence".
        for column_index in range(output_columns_length - len(confidence_columns), output_columns_length):
            outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                'name': 'confidence',
            }, source=source)

        return outputs_metadata

    @classmethod
    def _produce_using_semantic_types_metadata(cls, inputs_metadata: metadata_base.DataMetadata, index_columns: typing.Sequence[int],
                                               target_columns: typing.Sequence[int], hyperparams: Hyperparams) -> metadata_base.DataMetadata:
        confidence_columns = cls._get_confidence_columns(inputs_metadata, hyperparams)

        output_columns = cls._get_columns(inputs_metadata, index_columns, target_columns, hyperparams) + confidence_columns

        outputs_metadata = utils.select_columns_metadata(inputs_metadata, output_columns, source=cls)

        if confidence_columns:
            outputs_metadata = cls._update_confidence_columns(outputs_metadata, confidence_columns, cls)

        return outputs_metadata

    def _produce_reconstruct(self, inputs: Inputs, reference: Inputs, index_columns: typing.Sequence[int], target_columns: typing.Sequence[int],
                             hyperparams: Hyperparams) -> Outputs:
        if not index_columns:
            reference_index_columns = utils.get_index_columns(reference.metadata)

            if not reference_index_columns:
                raise ValueError("Cannot find an index column in reference data, but index column is required.")

            filtered_index_columns = self._filter_index_columns(reference.metadata, reference_index_columns, hyperparams)
            index = utils.select_columns(reference, filtered_index_columns, source=self)
        else:
            filtered_index_columns = self._filter_index_columns(inputs.metadata, index_columns, hyperparams)
            index = utils.select_columns(inputs, filtered_index_columns, source=self)

        if not target_columns:
            if index_columns:
                raise ValueError("No target columns in input data, but index column(s) present.")

            # We assume all inputs are targets.
            targets = inputs

            # We make sure at least basic metadata is generated correctly, so we regenerate metadata.
            targets.metadata = targets.metadata.set_for_value(targets, generate_metadata=True, source=self)

            # We set target column names from the reference. We set semantic types.
            targets.metadata = self._update_targets_metadata(targets.metadata, self._get_target_names(reference.metadata), self)

        else:
            targets = utils.select_columns(inputs, target_columns, source=self)

        return utils.append_columns(index, targets, source=self)

    @classmethod
    def _produce_reconstruct_metadata(cls, inputs_metadata: metadata_base.DataMetadata, reference_metadata: metadata_base.DataMetadata,
                                      index_columns: typing.Sequence[int], target_columns: typing.Sequence[int], hyperparams: Hyperparams) -> metadata_base.DataMetadata:
        if not index_columns:
            reference_index_columns = utils.get_index_columns(reference_metadata)

            if not reference_index_columns:
                raise ValueError("Cannot find an index column in reference data, but index column is required.")

            filtered_index_columns = cls._filter_index_columns(reference_metadata, reference_index_columns, hyperparams)
            index_metadata = utils.select_columns_metadata(reference_metadata, filtered_index_columns, source=cls)
        else:
            filtered_index_columns = cls._filter_index_columns(inputs_metadata, index_columns, hyperparams)
            index_metadata = utils.select_columns_metadata(inputs_metadata, filtered_index_columns, source=cls)

        if not target_columns:
            if index_columns:
                raise ValueError("No target columns in input data, but index column(s) present.")

            # We assume all inputs are targets.
            targets_metadata = inputs_metadata

            # We set target column names from the reference. We set semantic types.
            targets_metadata = cls._update_targets_metadata(targets_metadata, cls._get_target_names(reference_metadata), cls)

        else:
            targets_metadata = utils.select_columns_metadata(inputs_metadata, target_columns, source=cls)

        return utils.append_columns_metadata(index_metadata, targets_metadata, source=cls)

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, reference: Inputs, timeout: float = None, iterations: int = None) -> base.MultiCallResult:  # type: ignore
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, reference=reference)

    @classmethod
    def _get_target_names(cls, metadata: metadata_base.DataMetadata) -> typing.List[typing.Union[str, None]]:
        target_names = []

        for column_index in utils.list_columns_with_semantic_types(metadata, ('https://metadata.datadrivendiscovery.org/types/TrueTarget',)):
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))

            target_names.append(column_metadata.get('name', None))

        return target_names

    @classmethod
    def _update_targets_metadata(cls, metadata: metadata_base.DataMetadata, target_names: typing.Sequence[typing.Union[str, None]], source: typing.Any) -> metadata_base.DataMetadata:
        targets_length = metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        if targets_length != len(target_names):
            raise ValueError("Not an expected number of target columns to apply names for. Expected {target_names}, provided {targets_length}.".format(
                target_names=len(target_names),
                targets_length=targets_length,
            ))

        for column_index, target_name in enumerate(target_names):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index), 'https://metadata.datadrivendiscovery.org/types/Target', source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index), 'https://metadata.datadrivendiscovery.org/types/PredictedTarget', source=source)

            # We do not have it, let's skip it and hope for the best.
            if target_name is None:
                continue

            metadata = metadata.update_column(column_index, {
                'name': target_name,
            }, source=source)

        return metadata

    def _encode_columns(self, inputs: Outputs) -> Outputs:
        """
        Encode numpy arrays of numbers into float vectors.
        """

        outputs = inputs
        target_columns = utils.list_columns_with_semantic_types(outputs.metadata, ('https://metadata.datadrivendiscovery.org/types/PredictedTarget',))

        for column_index in target_columns:
            structural_type = outputs.metadata.query_column(column_index).get('structural_type', None)

            if structural_type is None:
                continue

            if not issubclass(structural_type, container.ndarray):
                continue

            new_column = []
            all_strings = True
            for value in outputs.iloc[:, column_index]:
                assert isinstance(value, container.ndarray)

                if value.ndim == 1:
                    new_column.append(','.join(str(v) for v in value))
                else:
                    all_strings = False
                    break

            if not all_strings:
                continue

            outputs_metadata = outputs.metadata
            outputs.iloc[:, column_index] = new_column
            outputs.metadata = outputs_metadata.update_column(column_index, {
                'structural_type': str,
                'dimension': metadata_base.NO_VALUE,
            }, source=self)
            outputs.metadata = outputs.metadata.remove((metadata_base.ALL_ELEMENTS, column_index, metadata_base.ALL_ELEMENTS), recursive=True, source=self)

        return outputs

    @classmethod
    def _encode_columns_metadata(cls, inputs_metadata: metadata_base.DataMetadata) -> metadata_base.DataMetadata:
        """
        Encode numpy arrays of numbers into float vectors, metadata only.
        """

        outputs_metadata = inputs_metadata
        target_columns = utils.list_columns_with_semantic_types(outputs_metadata, ('https://metadata.datadrivendiscovery.org/types/PredictedTarget',))

        for column_index in target_columns:
            structural_type = outputs_metadata.query_column(column_index).get('structural_type', None)

            if structural_type is None:
                continue

            if not issubclass(structural_type, container.ndarray):
                continue

            all_strings = True
            for value_index in range(outputs_metadata.query(())['dimension']['length']):
                value_metadata = outputs_metadata.query((value_index, column_index))

                assert 'structural_type' not in value_metadata or issubclass(value_metadata['structural_type'], container.ndarray)

                if 'dimension' in value_metadata and 'dimension' not in outputs_metadata.query((value_index, column_index, metadata_base.ALL_ELEMENTS)):
                    pass
                else:
                    all_strings = False
                    break

            if not all_strings:
                continue

            outputs_metadata = outputs_metadata.update_column(column_index, {
                'structural_type': str,
                'dimension': metadata_base.NO_VALUE,
            }, source=cls)
            outputs_metadata = outputs_metadata.remove((metadata_base.ALL_ELEMENTS, column_index, metadata_base.ALL_ELEMENTS), recursive=True, source=cls)

        return outputs_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]], hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if method_name != 'produce':
            return output_metadata

        if 'inputs' not in arguments or 'reference' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])
        reference_metadata = typing.cast(metadata_base.DataMetadata, arguments['reference'])

        index_columns = utils.get_index_columns(inputs_metadata)
        target_columns = utils.list_columns_with_semantic_types(inputs_metadata, ('https://metadata.datadrivendiscovery.org/types/PredictedTarget',))

        if index_columns and target_columns:
            outputs_metadata = cls._produce_using_semantic_types_metadata(inputs_metadata, index_columns, target_columns, hyperparams)
        else:
            outputs_metadata = cls._produce_reconstruct_metadata(inputs_metadata, reference_metadata, index_columns, target_columns, hyperparams)

        outputs_metadata = cls._encode_columns_metadata(outputs_metadata)

        return outputs_metadata
