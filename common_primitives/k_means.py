import os
from typing import Dict, Union
from d3m.container.numpy import ndarray

from sklearn.cluster import KMeans as KMeans_  # type: ignore

from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.types import Data

import common_primitives

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
Inputs = ndarray
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.


class Params(params.Params):
    cluster_centers: ndarray  # Coordinates of cluster centers.


class Hyperparams(hyperparams.Hyperparams):
    n_clusters = hyperparams.Hyperparameter[int](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
                            default=8,
                            description='Number of clusters to use by default for :meth:`k_means` queries. '
                            )
    init = hyperparams.Hyperparameter[Union[str, ndarray]](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default='k-means++',
                            description='"k-means++" selects initial cluster centers for k-mean clustering in a smart way to speed up convergence,'
                                        ' "random" chooses k observations at random for initial centroids'
                                        ' can also pass in an ndarray of shape (n_clusters, n_features) that gives the initial centroids'
                            )
    n_init = hyperparams.Hyperparameter[int](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default=10,
                            description='Number of time the k-means algorithm will be run with different centroid seeds.'
                                        ' The final results will be the best output of n_init consecutive runs in terms of inertia.'
                            )
    max_iter = hyperparams.Hyperparameter[int](
                            semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter'],
                            default=300,
                            description='max number of iterations for single run'
                            )


class KMeans(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive wrapping an existing sklearn clustering algorithm.
    """

    __author__ = 'Oxford DARPA D3M Team, Rob Zinkov <zinkov@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
        'id': '30ac35ef-b4af-4396-b048-a75afae68068',
        'version': '0.1.0',
        'name': 'sklearn.neighbors.classification.KNeighborsClassifier',
        'source': {
            'name': common_primitives.__author__,
            'contact': 'mailto:zinkov@robots.ox.ac.uk',
        },
        'installation': [{'type': 'PIP',
                          'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                              git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                          )}],
        'python_path': 'd3m.primitives.common_primitives.KMeans',
        'algorithm_types': ['K_MEANS_CLUSTERING'],
        'primitive_family': 'CLUSTERING'
    })

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed)

        self._clf = KMeans_(n_clusters=hyperparams['n_clusters'],
                            init=hyperparams['init'],
                            n_init=hyperparams['n_init'],
                            max_iter=hyperparams['max_iter'],
                            random_state=random_seed)
        self._training_inputs = None  # type: Inputs
        self._fitted = False

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        return CallResult(self._clf.predict(inputs))

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        self._training_inputs = inputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        # TODO use minibatches, which can now handle iterations and timeout corectly
        # If already fitted with current training data, this call is a noop.
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        self._clf.fit(self._training_inputs)
        self._fitted = True

        return CallResult(None)

    def get_params(self) -> Params:
        return Params(
                    cluster_centers=self._clf.cluster_centers)

    def set_params(self, *, params: Params) -> None:
        self._clf.cluster_centers = params.cluster_centers
